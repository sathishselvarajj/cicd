package com.sathish.cicd.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CicdController {

	@GetMapping("/{name}")
	public String getUserName(@PathVariable String name) {
		return "Hi, Welcome "+name;
	}
	
	@GetMapping("/id/{id}")
	public String getUserid(@PathVariable String id) {
		return "Hi, Welcome "+id;
	}
	
//NvgUNjmyAFcMqg2AZzfn	
}
