package com.sathish.cicd;


import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class CicdApplicationTests {

	
	@Autowired
	private MockMvc mvc;

	@Test
	public void getUserName() throws Exception {
		mvc.perform(MockMvcRequestBuilders.get("/sathish").accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(content().string("Hi, Welcome sathish"));
	}
	
	@Test
	public void getUserId() throws Exception {
		mvc.perform(MockMvcRequestBuilders.get("/id/2002").accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(content().string("Hi, Welcome 2002"));
	}

}
